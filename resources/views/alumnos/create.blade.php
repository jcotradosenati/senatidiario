<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
        <h2>Crear Alumno</h2>
        <form method="POST" action="{{ route('alumnos.store') }}">
            @csrf

            <input type="text" id="nombres" name="nombres" value="{{ old('nombres')}}"/>
            @error('nombres')
                    <div>{{ $message }}</div>
            @enderror
            <input type="text" id="apellidos" name="apellidos" value="{{ old('apellidos')}}"/>
            @error('apellidos')
                    <div>{{ $message }}</div>
            @enderror
            <input type="text" id="dni" name="dni" value="{{ old('dni')}}"/>
            @error('dni')
                    <div>{{ $message }}</div>
            @enderror
                
            <button type="submit">Guardar</button>
        </form>

        @if(session('error'))
                {{ session('error') }}
        @endif
</body>
</html>