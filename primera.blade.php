<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <script>
        function botonclick(){
            
            var nombres = document.getElementById("nombres").value;
            var apellidos = document.getElementById("apellidos").value;
            
            var linkElement = document.getElementById('enlace');
            linkElement.href="segunda/"+encodeURI(nombres)+"/"+encodeURI(apellidos);
            console.log(linkElement);
            linkElement.click();
                 
        }
    </script>

</head>
<body>
                            {{--  No se esta usando el URL del navegador --}}    
                            {{--  http://x.x.x.x:pp/segunda --}}
    <form method="POST" action="{{ route('nom_segunda') }}">

        @csrf
        <input type="text" name="nombres" id="nombres" />
        <input type="text" name="apellidos" id="apellidos" />
        <input type="text" name="dni" id="dni" />
        
        <button type="submit" class="btn btn-primary">Ir a segunda</button>
    </form>
    {{-- Que se muestre en la nueva vista tercera.blade.php --}}
    <div style="border:1px solid black">
    <form method="POST" action="">

        @csrf
        <input type="text" name="curso" id="curso" />
        <input type="text" name="anio" id="anio" />
        <input type="text" name="ciclo" id="ciclo" />
        
        <button type="submit" class="btn btn-primary">Ir a tercera</button>
    </form>
    </div>
       
</body>
</html>