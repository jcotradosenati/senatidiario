<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Alumno;
use App\Models\Matricula;
class MatriculaController extends Controller
{
    public function testCreate(Request $request)
    {
        /*
        $alumno = new Alumno();
        $alumno->dni = "87456511";
        $alumno->nombres = "jorge";
        $alumno->apellidos = "cotrado";
 
        $alumno->save();
        */
        if(session("idAlumno")){//si la session existe
            $idAlumno = session("idAlumno");
            
            $matricula = new Matricula();
            $matricula->anioAcad= "2023-I";
            $matricula->idAlumno = $idAlumno;
            //$matricula->idCurso=1;//ESTO DEPENDE DE TU DB
            $matricula->save();
    
            return "CREADO MATRICULA OK";
        }
        else{
            return "NO SE ENCONTRO SESION";    
        }
        
    }
}
