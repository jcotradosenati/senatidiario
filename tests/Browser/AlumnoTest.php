<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class AlumnoTest extends DuskTestCase
{
    
    use DatabaseMigrations;
    
    public function testCreate(): void
    {
        $this->browse(function (Browser $browser) {

            $browser->visit('/alumnos/create')
                ->type('dni', '70633367') // Ingresa el DNI del alumno
                ->type('nombres', 'Juan') // Ingresa los nombres del alumno
                ->type('apellidos', 'Pérez') // Ingresa los apellidos del alumno
                ->pause(2000)
                ->press('Guardar') // Envía el formulario
                ->assertPathIs('/alumnos');

            $browser->pause(2000);
                
        });
        

    }
}
