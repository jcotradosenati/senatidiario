<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AlumnoTest extends TestCase
{
    use RefreshDatabase;
    public function test_index(): void
    {
        $response = $this->get('/alumnos');//https://xxxxx:8000/alumnos
        $response->assertStatus(200);
        $response->assertViewIs('alumnos.index');
    }
    public function test_create(): void
    {
        
        $alumnoData = [
            'nombres' => 'Juan',
            'apellidos' => 'Perez',
            'dni'=>'67564332'
        ];
        $response = $this->post(route('alumnos.store'), $alumnoData);
        $response->assertStatus(302);
        $response->assertRedirect(route('alumnos.index'));
        
        $this->assertDatabaseHas('alumnos', [
            'nombres' => 'Juan',
            'apellidos' => 'Perez',
            'dni'=>'67564332'
        ]);
    }
    public function test_create_validation(): void
    {

        $alumnoData = [
            'nombres' => '',
            'apellidos' => '',
        ];
        $response = $this->post(route('alumnos.store'), $alumnoData);
        $response->assertStatus(302);
        $response->assertSessionHasErrors([
            'nombres',
            'apellidos',
        ]);

        ////
        $dni_errors=array('','56767','786786671','40633367');
        foreach($dni_errors as $dni){

            $alumnoData = [
                'dni' => $dni,
            ];
            $response = $this->post(route('alumnos.store'), $alumnoData);
            $response->assertStatus(302);
            $response->assertSessionHasErrors([
                'dni',
            ]);

        }//end foreach

    }//end function
}
